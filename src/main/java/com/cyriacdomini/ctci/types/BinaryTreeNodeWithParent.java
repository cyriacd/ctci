package com.cyriacdomini.ctci.types;

public class BinaryTreeNodeWithParent<T>{
    public T data;
    public BinaryTreeNodeWithParent<T> left;
    public BinaryTreeNodeWithParent<T> right;
    public BinaryTreeNodeWithParent<T> parent;
    public BinaryTreeNodeWithParent(T value){
        data = value;
        left = null;
        right = null;
        parent = null;
    }
    public void setRight(T data){
        this.right = new BinaryTreeNodeWithParent<T>(data);
        this.right.parent = this;
    }
    public void setLeft(T data){
        this.left = new BinaryTreeNodeWithParent<T>(data);
        this.left.parent = this;
    }
} 